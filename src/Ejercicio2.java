
import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Ejercicio2 {
    public static void main(String[] args) {
        String command = "java -jar C:\\Users\\Usuario\\IdeaProjects\\PSP\\Random10\\out\\artifacts\\Random10_jar\\Random10.jar";
        List<String> argsList = new ArrayList<>(Arrays.asList(command.split(" ")));

        ProcessBuilder pb = new ProcessBuilder(argsList);
        pb.inheritIO();
        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);

        try {
            Process p = pb.start();
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}
