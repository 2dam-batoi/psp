import java.io.*;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLOutput;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.zip.InflaterInputStream;

public class Ejercicio1 {

    public static void main(String[] args){

        if(args.length <= 0 ){
            System.err.println("Se necesita un comando/programa para ejecutar");
            System.exit(-1);
        }

        List<String> lista = Arrays.asList(args);
        ProcessBuilder pb = new ProcessBuilder(lista);
        File f = new File("output.txt");

        FileOutputStream fileOutputStream;
        DataOutputStream dataOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(f,true);
            dataOutputStream = new DataOutputStream(fileOutputStream);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }

        try{
            Process process = pb.start();

            InputStream inputStream = process.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            if(!process.waitFor(2, TimeUnit.SECONDS)){
                System.out.println("Se ha excedido el tiempo");
                System.exit(-1);
            }
            String line;

            while ((line = bufferedReader.readLine()) != null){
                System.out.println(line);
                dataOutputStream.writeUTF(line);
            }

        }catch (IOException ex){
            System.err.println("IO Exception");
            System.exit(-1);
        }catch (InterruptedException ex){
            System.err.println("Proceso ejecutado con errores");
            System.exit(-1);
        }

    }

}
