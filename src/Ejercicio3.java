import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio3 {

    public static void main(String[] args) {
        String command = "java -jar C:\\Users\\Usuario\\IdeaProjects\\PSP\\Minusculas\\out\\artifacts\\Minusculas_jar\\Minusculas.jar";
        List<String> argsList = new ArrayList<>(Arrays.asList(command.split(" ")));

        ProcessBuilder pb = new ProcessBuilder(argsList);
        try {
            Process p = pb.start();

            OutputStream os = p.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            Scanner procesoSC = new Scanner(p.getInputStream());
            Scanner scanner = new Scanner(System.in);

            String line = scanner.nextLine().toLowerCase();

            while (!line.equals("finaliza")){
                bw.write(line);
                bw.newLine();
                bw.flush();
                System.out.println(procesoSC.nextLine());
                line = scanner.nextLine().toLowerCase();
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
