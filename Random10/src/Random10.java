import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Random10{

    public static void main(String[] args) throws IOException {

        File f = new File("random.txt");

        FileOutputStream fos;
        DataOutputStream dataOutputStream = null;
        int num;


        try {
            fos = new FileOutputStream(f,true);
            dataOutputStream = new DataOutputStream(fos);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();



        try {
            while (!line.equals("close")){
                num = (int) (Math.random() * 10) +1 ;
                System.out.println(num);
                dataOutputStream.writeInt(num);
                line = scanner.nextLine();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }


    }
}
